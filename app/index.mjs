import express from 'express';
import request from 'request';
import ssr from './ssr.mjs';

const app = express();

const host = 'https://dev.meixin.57blocks.io';

app.get('/static/*', async (req, res) => {
    request(`${host}${req.url}`).pipe(res);
});

app.get('/manifest.json', async (req, res) => {
    request(`${host}${req.url}`).pipe(res);
});

app.get('/favicon.ico', async (req, res) => {
    request(`${host}${req.url}`).pipe(res);
});

app.get('*', async (req, res, next) => {
  // const {html, ttRenderMs} = await ssr(`${req.protocol}://${req.get('host')}/index.html`);
  const {html, ttRenderMs} = await ssr(`${host}${req.originalUrl}`);
  // Add Server-Timing! See https://w3c.github.io/server-timing/.
  res.set('Server-Timing', `Prerender;dur=${ttRenderMs};desc="Headless render time (ms)"`);
  return res.status(200).send(html); // Serve prerendered page as response.
});

app.listen(8000, () => console.log('Server started. Press Ctrl+C to quit'));
